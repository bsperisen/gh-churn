from flask import render_template
from flask_app import app
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
import pandas as pd
from flask import request
from a_Model import ModelIt
from predict import predict_sc18cat

@app.route('/')
@app.route('/index')
def index():
    return render_template("input.html")


@app.route('/input')
def cesareans_input():
    return render_template("input.html")

from regression import run_OLS

@app.route('/output')
def cesareans_output():
    #pull 'birth_month' from input field and store it
    repo_url = request.args.get('repo_url')

    return_tuple = predict_sc18cat(repo_url, n_samples=300000)

    if return_tuple is None:
        return render_template("output.html", prediction=None,
                               explan_list=None)
    else:
        output_tuple, cached = return_tuple
        proba, proj_row_df, translated_exp = output_tuple
        print('type', type(translated_exp), len(translated_exp), str(translated_exp))
        print('prediction:', proba)
        return render_template("output.html", prediction=round(proba[1]*100, 1),
                               explan_list=translated_exp)


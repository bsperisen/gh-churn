import pandas as pd
import os
from git import Repo
import numpy as np
import math

def convert_api_to_git_url(api_url):
    return api_url[0:8] + api_url[12:23] + api_url[29:] + ".git"

def insert_token(url_str, gtoken):
    return url_str[0:8] + gtoken + '@' + url_str[8:]

# Note: this is copied from the loadbalance function from supergametools
# Returns a list of the number of g values assigned to processors.
def loadbalance(n, p):
    inc = int(n/p)
    R = n - inc*p

    load = [int(inc) for i in range(p)]

    for i in range(int(R)):
        load[i] += 1

    load.append(0)
    load.sort()
    print('load', load)
    return load

if __name__ == "__main__":
    with open('gh-token') as gh_token_file:
        gh_token = gh_token_file.read()

    sample_file = 'parpull/sample_to_clone.csv'
    sample = pd.read_csv('sample_to_clone.csv')
    clone_dir = 'parpull/'

    prev_clone_dir = 'repos/'

    i = 0
    limit = 100000
    existing_repos = os.listdir(clone_dir) + os.listdir(prev_clone_dir)

    n_to_clone = sample.shape[0]
    print('sample size:' + str(n_to_clone))

    # Parallelize over different g's
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    slices = loadbalance(n_to_clone, size)
    print('size', size)
    cumslices = np.cumsum(np.array(slices))
    slice = slices[rank+1] # slices contains the number of jobs each process will do.

    row_start = cumslices[rank]
    row_end = row_start + slice
    sample_slice = sample.iloc[row_start:row_end, :]

    for index, row in sample_slice.iterrows():
        # print('proc ', rank, 'doing ', index)
        # continue
        if i >= limit:
            break

        dir_name = row['name'] + '_' + str(row['project_id'])
        if dir_name in existing_repos:
            print(dir_name + ' already cloned')

        else:
            print('Proc ' + str(rank) + ' job ' + str(i+1) + '/' + str(slice) + ' cloning: ' + dir_name)
            i += 1
            api_url = row['url']
            try:
                shortcut_url = insert_token(convert_api_to_git_url(api_url), gh_token)
                Repo.clone_from(shortcut_url, clone_dir + dir_name)
                print("\tSuccessfully cloned " + dir_name)
                continue

            except Exception as error:
                print("\tShortcut failed to clone " + dir_name + ': ' + str(error))

import pandas as pd
import math
from common_funcs import *
import numpy as np

repo_features_file = 'repo_features.csv'
commit_counts_file = 'commit_counts.csv'

repo_features = remove_unnamed_cols(pd.read_csv(repo_features_file))
commit_counts = remove_unnamed_cols(pd.read_csv(commit_counts_file))

# print(repo_features.head().to_string())
repo_features.index = repo_features['project_id']
commit_counts.index = commit_counts['project_id']
repo_features = repo_features.drop('project_id', axis=1)
commit_counts = commit_counts.drop(['project_id', 'name'], axis=1)

main_table = repo_features.join(commit_counts)
main_table.to_csv('main_table_raw.csv')
main_table.loc[:, 'sec_commits18_times4'] = main_table.loc[:, 'sec_commits18'] * 4 # Extrapolate for this year

# Discretize contributions

max_sc = max(main_table['sec_commits17'].max(), main_table['sec_commits18_times4'].max())
# This was some automatic binning that's commented out
# max_l2_sc = math.floor(math.log(max_sc, 2)) + 1
#bins = [0] + [16**i for i in range(max_l2_sc + 1)]
bins = [0, 10, 100, max_sc+2] # Ad-hoc
main_table['sc17_cat'] = pd.cut(main_table['sec_commits17'], bins, right=False, labels=range(len(bins)-1))
main_table['sc18_cat'] = pd.cut(main_table['sec_commits18_times4'], bins, right=False, labels=range(len(bins)-1))

# Look at the diff
main_table['sc_diff'] = main_table['sec_commits18_times4'] - main_table['sec_commits17']
main_table['sc_diff_sign'] = (main_table['sc_diff'] > 0) * 1

# Drop all the weird empty columns
# TODO figure out why they show up
empty_cols = [col for col in main_table.columns if ('.1' in col)]
main_table = main_table.drop(empty_cols, axis=1)

# Get all code features averaged by length.
code_features = [col for col in list(main_table.columns) if 'code_' in col]
for feature in code_features:
    main_table[feature + '_avg'] = main_table[feature] / main_table['code_loc']

main_table = main_table.dropna() # Need to delete rows with zero lines of code

# Add log main contributors
main_table['l_sc17'] = np.log(main_table['sec_commits17'] + 0.01)
main_table['l_mc'] = np.log(main_table['main_commits'] + 0.01)

# Classification into 3 categories by percentage change
main_table['diff_cat'] = 0
n_rows, n_cols = main_table.shape
for i, row in main_table.iterrows():
    sc17 = row['sec_commits17']
    sc18 = row['sec_commits18']
    perc_threshold = 0.5
    perc_change = (sc18 - sc17) / sc17
    if perc_change < -perc_threshold:
        main_table.at[i, 'diff_cat'] = 0
    elif -perc_threshold <= perc_change <= perc_threshold:
        main_table.at[i, 'diff_cat'] = 1
    else:
        main_table.at[i, 'diff_cat'] = 2

main_table['sc18_cat'] = (main_table['sec_commits18'] > 0) * 1

main_table['lsc17'] = np.log(main_table['sec_commits17'] + 0.01)
main_table['lsc18'] = np.log(main_table['sec_commits18_times4'] + 0.01)

#TODO Warning: you're removing all projects with contributors <= 4
smalls_threshold = 4
smalls_main_table = main_table[main_table['sec_commits17'] <= smalls_threshold]
bigs_main_table = main_table[main_table['sec_commits17'] <= smalls_threshold]
# print(main_table.sc17_cat.max())
main_table.sort_values(by='sec_commits18_times4', ascending=False).to_csv('main_table.csv')
smalls_main_table.to_csv('smalls_main_table.csv')
bigs_main_table.to_csv('bigs_main_table.csv')
pass
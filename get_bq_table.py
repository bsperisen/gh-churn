import numpy as np
import matplotlib.pylab as plt
import pandas as pd
from google.cloud import bigquery
from bq_helper import BigQueryHelper
import os
import urllib.request, json

mydataset_name = 'MyDataset'
ght_dataset_name = 'ghtorrent-bq.ght_2018_04_01'
ght_bq = BigQueryHelper('ghtorret-bq', ght_dataset_name.split('.')[1])

table_name = 'PyNotCollabs'

query = """
SELECT *
FROM `{0}.{1}`
""".format(mydataset_name,table_name)
sample_df = ght_bq.query_to_pandas(query)

sample_df.to_csv(table_name + '.csv')

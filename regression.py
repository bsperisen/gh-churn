import pandas as pd
import glob
from google.cloud import bigquery
from bq_helper import BigQueryHelper
import os
import numpy as np

import sklearn
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.externals import joblib

from sklearn.metrics import roc_curve

import statsmodels.api as sm
import matplotlib.pyplot as plt

from common_funcs import *

import itertools

from imblearn.over_sampling import SMOTE

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def run_OLS(proj_suffix=None):
    main_table = remove_unnamed_cols(pd.read_csv('main_table.csv'))
    lm = linear_model.LinearRegression()
    # X = main_table[['main_commits', 'sec_commits17', 'readme_len']]
    X = main_table
    X = X.assign(log_sec_commits17 = np.log(X['sec_commits17'] + 1))
    X = X.assign(log_main_commits = np.log(X['main_commits'] + 1))
    X = X.assign(rd2 = np.power(X['readme_len'], 2))

    y = main_table['sec_commits18']
    y = y.rename('sec_commits18')

    model = sm.OLS(y, sm.add_constant(X)).fit()
    print(model.summary())


    proj_url = 'https://api.github.com/repos/' + proj_suffix
    proj_rows = main_table.loc[main_table['url'] == proj_url]
    n_rows = proj_rows.shape[0]
    if n_rows == 0:
        return (None, None, None)
    proj_row = proj_rows.iloc[0]
    proj_row_df = pd.DataFrame()
    proj_row = proj_row_df.append(proj_row,ignore_index=True)

    n_rows = proj_rows.shape[0]
    # if n_rows == 0:
    #     return (None, None, None)

    proj_X = proj_row[['main_commits', 'sec_commits17', 'readme_len']]
    proj_X = pd.DataFrame(proj_X)

    proj_X = proj_X.assign(log_sec_commits17 = np.log(proj_X['sec_commits17'] + 1))
    proj_X = proj_X.assign(log_main_commits = np.log(proj_X['main_commits'] + 1))
    proj_X = proj_X.assign(rd2 = np.power(proj_X['readme_len'], 2))


    temp_dict = {'const':1.0}
    for col in proj_X.columns:
        temp_dict[col] = proj_X.iloc[0][col]

    # temp_dict
    new_proj_X = pd.DataFrame(temp_dict, index= [0])
    proj_X = new_proj_X

    prediction = 4 * model.predict(proj_X.values)


    # n_samples = len(X)
    # cens = np.ones(n_samples)
    # tm = TobitModel()
    # tm.fit(X, y, cens=pd.Series(cens), verbose=True)

    # prediction = model.predict(proj_row = [])

    return (prediction, proj_X, model)

    def run_logistic(proj_suffix=None):
        main_table = remove_unnamed_cols(pd.read_csv('main_table.csv'))
        lm = linear_model.LinearRegression()
        # X = main_table[['main_commits', 'sec_commits17', 'readme_len']]
        X = main_table
        X = X.assign(log_sec_commits17=np.log(X['sec_commits17'] + 1))
        X = X.assign(log_main_commits=np.log(X['main_commits'] + 1))
        X = X.assign(rd2=np.power(X['readme_len'], 2))

        y = main_table['sec_commits18']
        y = y.rename('sec_commits18')

        model = sm.OLS(y, sm.add_constant(X)).fit()
        print(model.summary())

        proj_url = 'https://api.github.com/repos/' + proj_suffix
        proj_rows = main_table.loc[main_table['url'] == proj_url]
        n_rows = proj_rows.shape[0]
        if n_rows == 0:
            return (None, None, None)
        proj_row = proj_rows.iloc[0]
        proj_row_df = pd.DataFrame()
        proj_row = proj_row_df.append(proj_row, ignore_index=True)

        n_rows = proj_rows.shape[0]
        # if n_rows == 0:
        #     return (None, None, None)

        proj_X = proj_row[['main_commits', 'sec_commits17', 'readme_len']]
        proj_X = pd.DataFrame(proj_X)

        proj_X = proj_X.assign(log_sec_commits17=np.log(proj_X['sec_commits17'] + 1))
        proj_X = proj_X.assign(log_main_commits=np.log(proj_X['main_commits'] + 1))
        proj_X = proj_X.assign(rd2=np.power(proj_X['readme_len'], 2))

        temp_dict = {'const': 1.0}
        for col in proj_X.columns:
            temp_dict[col] = proj_X.iloc[0][col]

        # temp_dict
        new_proj_X = pd.DataFrame(temp_dict, index=[0])
        proj_X = new_proj_X

        prediction = 4 * model.predict(proj_X.values)

        # n_samples = len(X)
        # cens = np.ones(n_samples)
        # tm = TobitModel()
        # tm.fit(X, y, cens=pd.Series(cens), verbose=True)

        # prediction = model.predict(proj_row = [])

        return (prediction, proj_X, model)

    # Comment out visualization

    # yX = pd.concat([X,y], axis=1)

    # yX.sort_values(by=['sec_commits17']).head()
    # yX.hist(column='readme_len', bins=50)
    #
    # yX = yX.assign(diffsc = (yX['sec_commits18'] - yX['sec_commits17']))
    # plt.hist(yX['diffsc'].values, bins=30, range=(-50, 50))
    # plt.show()
    # yX.sort_values(by='readme_len').plot.scatter(x='main_commits', y='sec_commits18')
    #
    # ax = yX.sort_values(by='readme_len').plot.scatter(x='readme_len', y='sec_commits18')
    # ax.set_ylim((0,30))
    # ax.set_xlim((0,250))
    #
    # yX = yX.assign(diff_sec_commits = X['log_sec_commits17'] - y)
    #
    # yX.plot.scatter(x='readme_len', y='log_sec_commits17')

def run_random_forest(main_table_file):
    main_table = remove_unnamed_cols(pd.read_csv(main_table_file))
    labels = np.array(main_table['sc_diff_sign'])

    labels = np.array(main_table['sc18_cat'])
    # Drop all the 2018 and useless variables
    features = main_table.drop(
        ['lsc18', 'diff_cat', 'sc_diff_sign', 'sc_diff', 'sc18_cat', 'lsc18', 'sec_commits18', 'sec_commits18_times4',
         'name',
         'project_id', 'url', 'cloned', 'code_classes_complexity', 'code_functions_complexity'], axis=1)

    # max_label = max(features['diff_cat'].max(), labels.max())
    feature_list = list(features.columns)
    features = np.array(features)

    features_train, features_test, labels_train, labels_test = train_test_split(features, labels, test_size=0.2,
                                                                                random_state=1)
    # features_train, features_val, labels_train, labels_val = train_test_split(features_train, labels_train, test_size=0.25, random_state=1)

    print('Training Features Shape:', features_train.shape)
    print('Training Labels Shape:', labels_train.shape)
    print('Testing Features Shape:', features_test.shape)
    print('Testing Labels Shape:', labels_test.shape)

    # sm = SMOTE(random_state=12)
    # features_resamp, labels_resamp = sm.fit_sample(features_train, labels_train)

    # Scaling
    X_train, X_test = (features_train, features_test)
    y_train, y_test = (labels_train, labels_test)

    scaler = preprocessing.StandardScaler().fit(X_train)
    X_test_scaled = scaler.transform(X_test)
    pipeline = make_pipeline(preprocessing.StandardScaler(),
                             RandomForestClassifier(n_estimators=100))

    hyperparameters = {'randomforestclassifier__max_features': ['auto', 'sqrt', 'log2'],
                       'randomforestclassifier__max_depth': [None, 5, 3, 1]}

    clf = GridSearchCV(pipeline, hyperparameters, cv=10)

    # Fit and tune model
    clf.fit(X_train, y_train)

    # The baseline predictions are the historical averages
    baseline_preds = X_test[:, feature_list.index('sc17_cat')]
    baseline_preds[:] = 0
    # Baseline errors, and display average baseline error
    baseline_errors = abs(baseline_preds - y_test)
    print('Average baseline error: ', round(np.mean(baseline_errors), 2))

    # rf = RandomForestClassifier(n_estimators=1000, random_state=42)
    # rf.fit(features_resamp, labels_resamp);

    # Use the forest's predict method on the test data
    y_preds = clf.predict(X_test)
    # predictions = rf.predict(features_val)
    # Calculate the absolute errors
    errors = abs(y_preds - y_test)
    # Print out the mean absolute error (mae)
    print('Mean Absolute Error:', round(np.mean(errors), 2))

    cnf_matrix = confusion_matrix(y_preds, y_test, labels=range(2))

    plt.figure()
    plt.interactive(False)
    plot_confusion_matrix(cnf_matrix, classes=range(2),
                          title='Confusion matrix, without normalization')
    plt.show()

    # Get numerical feature importances
    classifier = clf.best_estimator_.steps[-1]
    forest = classifier[1]
    importances = list(forest.feature_importances_)

    # clf.best_params_

    # List of tuples with variable and importance
    feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
    # Sort the feature importances by most important first
    feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)
    # Print out the feature and importances
    [print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];

    joblib.dump(clf, 'rf_classifier.pkl')
    pass

if __name__ == "__main__":
    # run_OLS('main_table.csv')
    # run_random_forest('main_table.csv')
    run_logistic('main_table.csv')

    # predict, proj_X, model = run_regression('galaxyproject/galaxy')
    # print('hello:', predict, proj_X)
    # print(model.summary())

import pandas as pd
import glob
from google.cloud import bigquery
from bq_helper import BigQueryHelper
import os
import numpy as np

import sklearn
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.externals import joblib

from sklearn.metrics import roc_curve

import statsmodels.api as sm
import matplotlib.pyplot as plt

from common_funcs import *

from lime import lime_text
from sklearn.pipeline import make_pipeline
import lime
import lime.lime_tabular
from lime.lime_tabular import LimeTabularExplainer

from git import Repo

import pickle

import clone_repos

import itertools

# Takes the list returned by Lime and turns it into a list of info that we can more or less directly display
# in the website.
# TODO redo doc, now it's a df
def translate_explanation(main_table, proj_id, exp_list):
    unhelpful_suggestion_columns = ['sec_commits17', 'l_sc17', 'lsc17', 'sc17_cat', 'l_mc', 'main_commits',
                                    'code_files_examined', 'code_files_examined_avg', 'pipeline_status']

    pick_recommendations = ['code_blank_avg', 'code_halstead_h2']

    clean_exp = []
    current_vals = []
    for exp_text, coeff in exp_list:
        text_chunks = exp_text.split()
        feature = [chunk for chunk in text_chunks if chunk[0].isalpha()][0]
        if feature not in unhelpful_suggestion_columns:
            clean_exp.append([feature, coeff])
            current_vals.append(main_table[main_table['project_id'] == proj_id].iloc[0][feature])


    stddevs = [main_table[feature].std() for feature, coeff in clean_exp]
    improvements = [stddevs[i] * clean_exp[i][1] for i in range(len(clean_exp))]
    df = pd.DataFrame()
    df['feature'] = [human_labels[clean_exp[i][0]] for i in range(len(clean_exp))]
    df['current_val'] = [round(val, 3) for val in current_vals]
    df['stddev'] = stddevs
    df['coeff'] = [clean_exp[i][1] for i in range(len(clean_exp))]
    df['improvement'] = improvements

    df['abs_improvement'] = np.abs(np.array(improvements))
    df = df.sort_values(by='abs_improvement', ascending=False)

    suggestions = []
    for i in range(len(clean_exp)):
        suggestions.append('Lower' \
            if clean_exp[i][1] < 0 \
            else 'Higher')

    df['suggest'] = suggestions
    df['percentile'] = 0. # TODO Need to implement
    return df

# TODO possibly better recommendation ranking based on smaller (10 percentile) change
def give10perc_improvement(main_table, proj_id, exp_df, num_recs = 3):
    print(main_table.head().to_string())
    improve10percs = []
    for index, row in exp_df.iterrows():
        feature = row['feature']
        sorted_main = main_table.sort_values(by=feature).reset_index()
        n_rows,n_cols = sorted_main.shape
        sorted_row = sorted_main[sorted_main['project_id'] == proj_id]
        percentile = int(sorted_row.iloc[0]['index']) / n_rows

        # TODO make this for improving
        # if row['coefficient'] > 0 and percentile < 0.9:
        #     improved_feature = sorted_row['feature'].quantile(percentile + 0.1)
        #     change = improved_feature - sorted_row.iloc[0][feature]
        #     improve10percs.append([])
        # elif row['coefficient'] > 0 and percentile < 0.9:
        #     pass

        print('index', sorted_row.index)
        break

    return exp_df

from build_repo_features import calc_features_on_repo_to_df
from parse_contrib_list import get_commit_counts

# Function to construct a row for a project not in our database, to be used to get prediction.
# Returns None if something goes wrong.
def construct_proj_row(suffix):
    try:
        with open('gh-token') as gh_token_file:
            gh_token = gh_token_file.read()

        clone_dir = 'new_repo_analysis/'
        dir_name = suffix
        shortcut_url = clone_repos.insert_token('https://github.com/' + suffix + '.git', gh_token)
        Repo.clone_from(shortcut_url, clone_dir + dir_name)
        repo_features_df = calc_features_on_repo_to_df(clone_dir + dir_name)
        main_commits, sec_commits = get_commit_counts(suffix)
    except:
        return None

    # TODO Need to finish building row
    return repo_features_df


# Function making prediction of project success
# Input: proj_suffix, n_samples
# Output: tuple (proba, proj_row_df, clf, translated_exp)
#   proba: probability of external contributors in 2018
#   proj_row_df: data frame with one row of features corresponding to the project
#   clf: random forest classifier
#   translated_exp: data frame with explanations, with columns
#     [feature, current_val, stddev, coeff, improvement, abs_improvement]
def predict_sc18cat(proj_suffix, num_recs=3, n_samples=1000):
    if '/' not in proj_suffix: # A '/' must be part of a valid suffix
        return None
    suffix_split = proj_suffix.split('/')
    cache_filename = 'cache/cache_' + suffix_split[0] + '.' + suffix_split[1]

    if os.path.isfile(cache_filename):
        output_tuple = pickle.load(open(cache_filename, 'rb'))

        # Round current values
        proba, proj_row_df, translated_exp = output_tuple
        current_vals = list(translated_exp['current_val'])
        current_vals = [round(val, 3) for val in current_vals]
        translated_exp['current_val'] = current_vals
        suggestions = []
        for index, row in translated_exp.iterrows():
            suggestions.append('<img src="https://upload.wikimedia.org/wikipedia/commons/0/04/Red_Arrow_Down.svg" style="width:20px;height:20px;"> Lower' \
                                   if row['coeff'] < 0 \
                                   else '<img src="https://upload.wikimedia.org/wikipedia/commons/8/8b/Green_Arrow_Up_Darker.svg" style="width:20px;height:20px;"> Higher')

        translated_exp['suggest'] = suggestions

        output_tuple = (proba, proj_row_df, translated_exp)

        cached = True
        return (output_tuple, cached)

    cached = False

    main_table = remove_unnamed_cols(pd.read_csv('main_table.csv'))
    print("columns: ", main_table.columns)
    proj_url = 'https://api.github.com/repos/' + proj_suffix
    proj_rows = main_table.loc[main_table['url'] == proj_url]
    n_rows = proj_rows.shape[0]

    # If this project is not in our cache, we need to grab it
    if n_rows == 0:
        proj_row = construct_proj_row(proj_suffix)
        return None
    else:
        proj_row = proj_rows.iloc[0]

    proj_row_df = pd.DataFrame()
    proj_id = proj_row['project_id']
    proj_name = proj_row['name']
    proj_row = proj_row_df.append(proj_row,ignore_index=True)

    # Drop all the 2018 and useless variables
    vars_to_drop = ['project_id', 'lsc18', 'diff_cat', 'sc_diff_sign', 'sc_diff', 'sc18_cat', 'lsc18', 'sec_commits18',
                    'sec_commits18_times4', 'name', 'url', 'cloned']
    proj_row = proj_row.drop(vars_to_drop, axis=1)

    main_table_pid = main_table.drop(vars_to_drop[1:], axis=1)
    main_table = main_table.drop(vars_to_drop, axis=1)
    X = np.array(main_table)

    # max_label = max(features['diff_cat'].max(), labels.max())
    feature_list = list(proj_row.columns)
    proj_row_df = proj_row
    proj_row = np.array(proj_row)

    proj_X = proj_row
    # proj_y = labels

    clf = joblib.load('rf_classifier.pkl')
    proba = clf.predict_proba(proj_X)

    explainer = LimeTabularExplainer(X, feature_names=main_table.columns)
    # print(proj_X.view())
    exp = explainer.explain_instance(X[0], clf.predict_proba, num_features=12, num_samples=n_samples)
    exp_list = exp.as_list()

    # probamain = clf.predict_proba(main_table)

    proba = proba.tolist()[0]

    translated_exp = translate_explanation(main_table_pid, proj_id, exp_list)
    # give10perc_improvement(main_table_pid, proj_id, translated_exp, num_recs=3)

    output_tuple = (proba, proj_row_df, translated_exp[:num_recs])
    pickle.dump(output_tuple, open(cache_filename, 'wb'))

    return (output_tuple, cached)

if __name__ == "__main__":
    main_table = remove_unnamed_cols(pd.read_csv('main_table.csv'))

    # Drop all the 2018 and useless variables
    vars_to_drop = ['lsc18', 'diff_cat', 'sc_diff_sign', 'sc_diff', 'sc18_cat', 'lsc18', 'sec_commits18',
                    'sec_commits18_times4', 'name', 'project_id', 'url', 'cloned']
    main_table = main_table.drop(vars_to_drop, axis=1)

    # test it on a particular project
    # Previously: galaxyproject/galaxy
    output_tuple, cached = predict_sc18cat('jgrynczewski/Assistive-Prototypes')
    proba, proj_X, exp_list = output_tuple
    print('length', len(exp_list))
    print(exp_list.to_string())
    pass

import pandas as pd
import glob
from google.cloud import bigquery
from bq_helper import BigQueryHelper
import os
from random import shuffle
import numpy as np
import sys

import radon
from radon.raw import analyze
import radon.metrics
from radon.visitors import ComplexityVisitor

import pylint.lint as lint

from timeout import timeout
import errno

from common_funcs import remove_unnamed_cols

repos_dir = 'parpull/'

# TODO Note readme_len, size flipped. Need to fix

#### Functions and variables for readme features ####
readme_feature_names = ['readme_len', 'readme_size', 'readme_headers',
                            'pipeline_status', 'url_count', 'img_count']

# Auxiliary function for file_counts below, taking open file descriptor f below
# TODO update description
def readme_counts_aux(f):
    pipeline_status = url_count = header_n = img_count = 0
    for i, l in enumerate(f):
#         print('here:', i, l)
        ls = l.strip()
        if len(ls) > 0:
            firstpart = ls.split(' ')[0]
            if ((ls == len(ls) * ls[0]) and (ls[0] == '-' or ls[0] == '=')) or \
                    (firstpart == len(firstpart) * '#'):
                header_n += 1
            pipeline_status += '[pipeline status]' in ls
            url_count += ls.count('http://') + ls.count('https://')
            img_count += ls.count('image::') + ls.count('figure::')

    return [i+1, header_n, pipeline_status, url_count, img_count]
    

# Input: file_name (str)
# Returns tuple: (number of lines in that file,
#                 number of lines with headers,
#                 number of urls,
#                 number of images)
# TODO Update description
def readme_counts(file_name):
    header_n = 0
    # If file isn't empty, count lines
    if os.stat(file_name).st_size > 0:
        # Try utf-8
        try:
            with open(file_name) as f:
                return readme_counts_aux(f)
                        
        except: # If fails, try latin1
            with open(file_name, encoding='latin1') as f:
                return readme_counts_aux(f)

    # If empty, return 0
    else:
        return [0] * (len(readme_feature_names) - 1)

    
# Input: dir_name (str): containing 'name_id' of project in repos folder
# Output: list of 2 ints: [readme_len, readme_size] for max values of readme file, defined as
# any file with name in the form README*
def calc_readme(dir_name):
    dir_name = repos_dir + dir_name
    readme_files = [file_name for file_name in glob.glob(dir_name + '/README*')
                    if os.path.isfile(file_name)]
    if not readme_files:
        return [0] * len(readme_feature_names)

    fcounts = [readme_counts(file) for file in readme_files]
    readme_lens = [fcount[0] for fcount in fcounts]
    longest = readme_lens.index(max(readme_lens))

    readme_size = os.path.getsize(readme_files[longest])
    return [readme_size] + fcounts[longest]

#### Code file features ####
code_feature_names = ['code_files', 'code_files_examined']
code_features_radon_raw = ['code_loc', 'code_lloc', 'code_sloc', 'code_comments', 'code_multi', 'code_blank',
                           'code_single_comments']
code_features_complexity = ['code_functions_complexity', 'code_classes_complexity', 'code_total_complexity']
code_features_halstead = ['code_halstead_h1', 'code_halstead_h2', 'code_halstead_N1', 'code_halstead_N2',
                          'code_halstead_vocabulary','code_halstead_length', 'code_halstead_calculated_length',
                          'code_halstead_volume', 'code_halstead_difficulty',
                          'code_halstead_effort', 'code_halstead_time', 'code_halstead_bugs']
code_features_pylint = ['code_pylint_score']
code_feature_names += code_features_radon_raw + code_features_complexity + code_features_halstead + code_features_pylint

# TODO add these in
code_features_avg = [name + '_avg' for name in code_features_radon_raw + code_features_complexity]

# TODO Update description
def code_counts_aux(f):
    pipeline_status = url_count = header_n = img_count = 0
    f_string = f.read()
    visitor = ComplexityVisitor.from_code(f_string)
    # print('complexity=', visitor.max_line, visitor.functions_complexity, visitor.classes_complexity, visitor.complexity)
    # print('raw=', radon.raw.analyze(f_string))
    raw_analysis = radon.raw.analyze(f_string)
    halstead_stats = radon.metrics.h_visit(f_string)
    # print(list(raw_analysis))
    return list(raw_analysis) + [visitor.functions_complexity, visitor.classes_complexity, visitor.complexity] + \
        list(halstead_stats)

# Takes a long time (maybe 1 sec/source file on avg) so disable this for now
def get_pylint_score(file_name):
    return 0 # lint.Run([file_name], exit=False).linter.stats['global_note']

# TODO Update description
@timeout(20, os.strerror(errno.ETIMEDOUT))
def code_counts(file_name):
    # print('about to count file:', file_name)
    # If file isn't empty, count lines
    if os.stat(file_name).st_size >= 10:
        # Try utf-8
        try:
            with open(file_name) as f:
                counts = code_counts_aux(f)
                pylint_score = get_pylint_score(file_name)
                return counts + [pylint_score]

        except:  # If fails, try latin1
            with open(file_name, encoding='latin1') as f:
                counts = code_counts_aux(f)
                pylint_score = get_pylint_score(file_name)
                return counts + [pylint_score]

    # If empty, return 0
    else:
        return [0] * (len(code_feature_names) - 2)

# TODO document
def calc_code(dir_name, max_files=10):
    # print('calculating code for ' + dir_name)
    code_files = 0
    stats_len = len(code_feature_names) - 2
    code_stats = np.zeros(stats_len)

    pyfiles = glob.glob(repos_dir + dir_name + '/**/*.py', recursive=True)
    code_files = len(pyfiles)
    if len(pyfiles) > max_files:
        shuffle(pyfiles)
    # print('finding # files', len(pyfiles))

    i = 0
    for filename in pyfiles:
        if i >= max_files:
            break
        file_itself = filename.split('/')[-1]
        # Skip installation files and really big or small files
        print('filesize:', os.stat(filename).st_size)
        if file_itself != 'setup.py' and \
                os.stat(filename).st_size >= 10 and os.stat(filename).st_size <= 5e5:
            try:
                code_stats += code_counts(filename)
                i += 1
            except (IndexError, SyntaxError, KeyError, SystemError, ValueError) as e:
                print("dependency error in " + filename + ", not counting. Error text: " + str(e))

    code_files_examined = i
    # print('about to divide:', code_stats, code_files_examined)
    if type(code_files_examined) is not int or code_files_examined < 1:
        return [0] * len(code_feature_names)
    else:
        code_stats_favg = code_stats / code_files_examined
        return [code_files, code_files_examined] + list(code_stats_favg)

# Class for defining new feature (more precisely, a list of related ones) from the actual repo
class RawFeature:
    # Input:
    # feature_names (list of str)
    # feature_func (function with 1 argument): 
    #    a function taking dir_name (a str as giving a directory located at repos/dir_name)
    #    returns list of the features (same len as feature_names)
    def __init__(self, feature_names, feature_func):
        self.names = feature_names
        self.data = {name:[] for name in feature_names}
        self.func = feature_func
        
    def append_repo(self, dir_name):
        feature_row = self.func(dir_name)
        i = 0
        for name in self.names:
            self.data[name].append(feature_row[i])
            i += 1

def write_local_df_to_csv(filename, local_df, new_project_ids, new_project_names, sfeatures):
    new_local_df = pd.DataFrame()
    new_local_df['project_id'] = new_project_ids
    new_local_df['name'] = new_project_names
    new_local_df = pd.concat([new_local_df] + [pd.DataFrame(sfeature.data) for sfeature in sfeatures], axis=1)
    combined_df = pd.concat([local_df, new_local_df], sort=False)
    combined_df.to_csv(filename)

# Calculates the features on a given repo and returns a DataFrame with them.
def calc_features_on_repo_to_df(directory):
    sfeatures = [RawFeature(readme_feature_names, calc_readme),
                 RawFeature(code_feature_names, calc_code)]

    for feature in sfeatures:
        feature.append_repo(directory)

    new_local_df = pd.DataFrame() # DataFrame we'll fill up and save to CSV, upload to bigquery
    new_local_df['project_id'] = 0
    new_local_df['name'] = 'filler_name'
    new_local_df = pd.concat([new_local_df] + [pd.DataFrame(sfeature.data) for sfeature in sfeatures], axis=1)
    return new_local_df


if __name__ == "__main__":
    # Get list of IDs
    existing_repos = os.listdir(repos_dir)
    # shuffle(existing_repos)

    local_df = pd.DataFrame() # DataFrame we'll fill up and save to CSV, upload to bigquery

    feature_file = 'repo_features.csv'
    save_feature_file = 'new_repo_features.csv'

    projects_done = []
    project_ids = []
    project_names = []
    if os.path.isfile(feature_file):
        local_df = remove_unnamed_cols(pd.read_csv(feature_file))
        projects_done = list(local_df['project_id'])
        project_ids = list(local_df['project_id'])
        project_names = list(local_df['name'])
        print('already done:', projects_done)

    elif os.path.isfile(save_feature_file):
        local_df = remove_unnamed_cols(pd.read_csv(save_feature_file))
        projects_done = list(local_df['project_id'])
        project_ids = list(local_df['project_id'])
        project_names = list(local_df['name'])
        print('already:', projects_done)


    sfeatures = [RawFeature(readme_feature_names, calc_readme),
                 RawFeature(code_feature_names, calc_code)]

    i = 0
    new_local_df = pd.DataFrame()
    new_project_ids = []
    new_project_names = []
    for directory in existing_repos:
        if directory[0] != '.': # Ignore any OS files (like .DS_store)
            print(str(i+1) + ' out of ' + str(len(existing_repos)) + ': ' + directory)
            project_id = int(directory.split('_')[-1])
            name = directory.split('_')[0]
            for piece in directory.split('_')[1:-1]:
                name += '_' + piece
            if project_id not in projects_done:
                new_project_ids.append(project_id)
                new_project_names.append(name)
                for feature in sfeatures:
                    feature.append_repo(directory)
                projects_done.append(project_id)
                write_local_df_to_csv(save_feature_file, local_df, new_project_ids, new_project_names, sfeatures)
        #         print("\tDone")
            else:
                print("\tAlready done")

            i += 1

    # write_local_df_to_csv(feature_file, local_df, project_ids, project_names, sfeatures)

    pass

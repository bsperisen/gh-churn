import pandas as pd
from google.cloud import bigquery
from bq_helper import BigQueryHelper
import os
import urllib.request, json

ght_dataset_name = 'ghtorrent-bq.ght_2018_04_01'
bq = BigQueryHelper('ghtorret-bq', ght_dataset_name.split('.')[1])

# Set up to save temporary queries
mydataset_name = 'MyDataset'
table_name = 'MainCommitCounts_unique'
client = bigquery.Client()
dataset_ref = client.dataset(mydataset_name)
dataset = bigquery.Dataset(dataset_ref)
try:
    dataset = client.create_dataset(dataset)
    dataset.location = 'US'
except:
    pass # No problem if it already
table_ref = dataset_ref.table(table_name)
table = bigquery.Table(table_ref)
try:
    table = client.create_table(table)
except:
    pass # No problem if it already exists

job_config = bigquery.QueryJobConfig()
job_config.destination = table
job_config.create_disposition = 'CREATE_IF_NEEDED'
job_config.write_disposition = 'WRITE_TRUNCATE'

query = """
SELECT * FROM `{0}.{1}`
""".format(mydataset_name, table_name)
df = bq.query_to_pandas(query)
df.to_csv('commit_counts.csv')

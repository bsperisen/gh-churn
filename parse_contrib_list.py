import json
from datetime import datetime
from dateutil.relativedelta import relativedelta

import subprocess

def get_commit_counts(project_suffix):
    bashCommand = 'curl -o new_repo_analysis/contrib_file_' + project_suffix  + ''' -H "Authorization: token `cat ../gh-token`"  https://api.github.com/repos/''' \
        + project_suffix + '/stats/contributors'
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    contrib_filename = 'new_repo_analysis/contrib_file_' + project_suffix

    with open(contrib_filename, 'r') as contrib_file:
        contrib_info = json.load(contrib_file)

    num_authors = len(contrib_info)

    # Fill author_commits with commit tallies for the previous year. Each element corresponds to a different author.
    author_commits = []
    for author_info in contrib_info:
        # print(len(author_info['weeks']))
        commit_sum = 0
        for week_info in author_info['weeks']:
            # If this week is within a year, tally up commits
            week_dt = datetime.fromtimestamp(week_info['w'])
            if relativedelta(week_dt, datetime.today()).years >= -1:
                commit_sum += week_info['c']
        author_commits.append(commit_sum)
            # how_long_ago = week_dt - datetime.today()
            # print('week:', week_dt, how_long_ago, relativedelta(week_dt, datetime.today()).years)

    author_commits.sort()
    main_commits = author_commits[-1]
    sec_commits = sum(author_commits[:-1])
    print('author_commits', author_commits)
    print('main:', main_commits, 'sec:', sec_commits)
    return (main_commits, sec_commits)
import pandas as pd

# Input: df (pd.DataFrame)
# Returns: that DataFrame with any columns containing 'Unnamed' removed.
def remove_unnamed_cols(df):
    unnamed_cols = [col for col in df.columns if col.find('Unnamed') != -1]
    return df.drop(unnamed_cols, 1)

# Function that does reverse of dataframe.to_csv()
# Input: filename of CSV file
# Returns: DataFrame with CSV file's unnamed columns removed
def my_pd_read_csv(filename):
    return remove_unnamed_cols(pd.read_csv(filename))

# Sorts DataFrame by column case insensitively
def sort_case_insensitive(df, column):
    return df.loc[df[column].str.lower().sort_values().index]

human_labels = {'project_id':'Project ID',
                'name':'Name',
                'readme_len':'README length',
                'readme_size':'README size',
                'readme_headers':'Number of README headings',
                'pipeline_status':'Indicator of Pipeline Status in README',
                'url_count':'Number of URLs in README (useful for documentation on other website)',
                'img_count':'Number of images in README',
                'code_files':'Number of source code files',
                'code_loc':'Lines of code per file',
                'code_lloc':'Logical lines of code per file',
                'code_sloc':'Source lines of code per file',
                'code_comments':'Comment lines per file',
                'code_multi':'Number of lines with multi-line strings per file',
                'code_blank':'Blank lines of code per file',
                'code_single_comments':'Number of lines that are just comments (no code) per file',
                'code_functions_complexity':'Cyclomatic complexity of functions',
                'code_classes_complexity':'Cyclomatic complexity of classes',
                'code_total_complexity':'Cyclomatic complexity',
                'code_halstead_h1':'Halstead h1',
                'code_halstead_h2':'Halstead h2',
                'code_halstead_N1':'Halstead N1',
                'code_halstead_N2':'Halstead N2',
                'code_halstead_vocabulary':'Halstead vocabulary',
                'code_halstead_length':'Halstead length',
                'code_halstead_calculated_length':'Halstead calculated length',
                'code_halstead_volume':'Halstead volume',
                'code_halstead_difficulty':'Halstead difficulty',
                'code_halstead_effort':'Halstead effort',
                'code_halstead_time':'Halstead time',
                'code_halstead_bugs':'Halstead bugs',
                'code_pylint_score':'Pylint score'}

keys_given_above = list(human_labels.keys())
for feature in keys_given_above:
    if 'code_' in feature:
        human_labels[feature + '_avg'] = human_labels[feature] + ' averaged per line'

keys_given_above = list(human_labels.keys())
for feature in keys_given_above:
    if '_complexity' in feature:
        human_labels[feature] += '<br>(<a href="https://en.wikipedia.org/wiki/Cyclomatic_complexity">learn more here</a>)'
    elif 'halstead' in feature:
        human_labels[feature] += '<br>(<a href="https://en.wikipedia.org/wiki/Halstead_complexity_measures">learn more here</a>)'
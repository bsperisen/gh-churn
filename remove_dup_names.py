import pandas as pd
import glob
from google.cloud import bigquery
from bq_helper import BigQueryHelper
import os
from random import shuffle
import numpy as np
import sys

import radon
from radon.raw import analyze
import radon.metrics
from radon.visitors import ComplexityVisitor

import pylint.lint as lint

from timeout import timeout
import errno

from common_funcs import *
from shutil import copyfile

repos_dir = 'minirepos/'
feature_file = 'repo_features.csv'
dupe_repos_dir = 'dupe_repos'

if __name__ == "__main__":
    # Get list of IDs
    existing_repos = os.listdir(repos_dir)
    # shuffle(existing_repos)

    local_df = remove_unnamed_cols(pd.read_csv(feature_file))[['name', 'project_id']]
    local_df = sort_case_insensitive(local_df, 'name')
    print(local_df.head(40).to_string())

    unique_names = local_df.groupby('name').agg('count').reset_index()
    unique_names = sort_case_insensitive(unique_names, 'name')
    print(unique_names.head(40).to_string())
    for index,row in unique_names.iterrows():
        if row['project_id'] > 1: # If count of project id's shows a dupe ...
            dupes = local_df[local_df['name'] == row['name']]
            project_ids = list(dupes['project_id'].sort_values(ascending=False))
            print('Here:', dupes.to_string())
            print('pids:', project_ids)
    pass
